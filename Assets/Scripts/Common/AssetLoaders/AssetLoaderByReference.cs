using Common.AssetLoaders.Base;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;

namespace Common.AssetLoaders
{
    public class AssetLoaderByReference<T> : BaseAssetLoader<T>
    {
        private readonly AssetReference _assetReference;
        private AsyncOperationHandle<T> _handle;

        public AssetLoaderByReference(AssetReference assetReference)
        {
            _assetReference = assetReference;
        }

        protected override AsyncOperationHandle<T> GetHandle()
        {
            return _assetReference.LoadAssetAsync<T>();
        }
        
    }
}
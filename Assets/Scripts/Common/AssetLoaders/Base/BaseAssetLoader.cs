using System;
using System.Threading.Tasks;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;

namespace Common.AssetLoaders.Base
{
    public abstract class BaseAssetLoader<T> : IAssetLoader<T>
    {
        private AsyncOperationHandle<T> _handle;
        protected abstract AsyncOperationHandle<T> GetHandle();

        
        public async Task<T> Load()
        {
            _handle = GetHandle();
            await _handle.Task;
            if (_handle.Status == AsyncOperationStatus.Succeeded)
            {
                return _handle.Result;
            }
            
            throw new NullReferenceException($"Asset reference isn't loaded {_handle.Status}");
        }


        public void Dispose()
        {
            if (_handle.IsValid())
            {
                Addressables.Release(_handle);
            }
        }
    }
}
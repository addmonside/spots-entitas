using System;
using System.Threading.Tasks;

namespace Common.AssetLoaders.Base
{
    public interface IAssetLoader<T> : IDisposable
    {
        Task<T> Load();
    }
}
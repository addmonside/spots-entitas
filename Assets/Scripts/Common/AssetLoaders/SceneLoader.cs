using System.Threading.Tasks;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.ResourceManagement.ResourceProviders;
using UnityEngine.SceneManagement;

namespace Common.AssetLoaders
{
    public class SceneLoader
    {
        private AsyncOperationHandle<SceneInstance> _handle;
        private readonly string _name;
        
        public bool IsLoaded => _handle.IsValid();

        public SceneLoader(string name)
        {
            _name = name;
        }
        
        public Task Load()
        {
            if (!IsLoaded)
            {
                _handle = Addressables.LoadSceneAsync(_name, LoadSceneMode.Additive);
            }
            return _handle.Task;
        }

        public Task Unload()
        {
            if (!IsLoaded) return Task.Run(() => { });
            _handle = Addressables.UnloadSceneAsync(_handle);
            return _handle.Task;

        }
    }
}
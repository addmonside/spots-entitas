using Asp.Runtime;
using UnityEngine;
using Zenject;

namespace Common.Widgets.Audio
{
    [RequireComponent(typeof(AudioSource))]
    public class MusicWidget: MonoBehaviour
    {
        private IMusicPlayer _player;

        [Inject]
        private void Construct(IMusicPlayer player)
        {
            _player = player;
        }
        
        private void Start()
        {
            var source = GetComponent<AudioSource>();
            _player.Play(source);
        }
    }
}
using Asp.Runtime;
using UnityEngine;
using Zenject;

namespace Common.Widgets.Audio
{
    [RequireComponent(typeof(AudioSource))]
    public class SoundWidget : MonoBehaviour
    {
        private ISfxPlayer _player;
        private AudioSource _source;

        [Inject]
        private void Construct(ISfxPlayer player)
        {
            _player = player;
        }
        
        private void Start()
        {
            _source = GetComponent<AudioSource>();
        }
        
        public void Play()
        {
            _player.Play(_source);
        }
        
    }
}
using System;
using Common.Services.Repository;
using UnityEngine;
using UnityEngine.Localization.Settings;
using Zenject;

namespace Common.Services.Localization
{
    [Serializable]
    public class LocaleIndexService : ILocaleIndexService, IInitializable
    {
        public int LocaleIndex 
        { 
            get => _localeIndex;
            set
            {
                _localeIndex = value;
                _settingsRepositoryService.Save(this);
            }
        }
        
        [SerializeField] private int _localeIndex;
        private readonly SettingsRepositoryService _settingsRepositoryService;
        
        public LocaleIndexService(SettingsRepositoryService settingsRepositoryService)
        {
            _settingsRepositoryService = settingsRepositoryService;
        }


        public async void Initialize()
        {
            _settingsRepositoryService.Load(this);
            await LocalizationSettings.InitializationOperation.Task;
            LocalizationSettings.SelectedLocale = LocalizationSettings.AvailableLocales.Locales[_localeIndex];
        }
    }
}

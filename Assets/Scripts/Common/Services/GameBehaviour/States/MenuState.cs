using System.Threading.Tasks;
using Common.Services.GameBehaviour.States.Base;
using Common.Services.SceneLoading.Constants;

namespace Common.Services.GameBehaviour.States
{
    public sealed class MenuState : SceneState
    {
        public MenuState(IStateSwitch stateSwitch) : base(SceneNames.MenuScene, stateSwitch) {}

        public override Task GoToGameplay()
        {
            IsUnloadScene = true;
            return StateSwitch.SwitchState<GameplayState>(true);
        }

        public override Task GoToSettings()
        {
            IsUnloadScene = false;
            return StateSwitch.SwitchState<SettingsState>();
        }
    }
}
using System.Threading.Tasks;
using Common.Services.GameBehaviour.States.Base;
using Common.Services.SceneLoading.Constants;

namespace Common.Services.GameBehaviour.States
{
    public class GameplayState : SceneState
    {
        public GameplayState(IStateSwitch stateSwitch) : base(SceneNames.GameplayScene, stateSwitch) {}
        
        public override async Task GoToMenu()
        {
            IsUnloadScene = true;
            await StateSwitch.SwitchState<MenuState>(true);
        }
        
        public override Task GoToSettings()
        {
            IsUnloadScene = false;
            return StateSwitch.SwitchState<SettingsState>();
        }
    }
}
using System.Threading.Tasks;
using Common.Services.GameBehaviour.States.Base;

namespace Common.Services.GameBehaviour.States
{
    public class StartupState : BaseState
    {
        public StartupState(IStateSwitch stateSwitch) : base(stateSwitch) {}

        public override Task GoToMenu()
        {
            StateSwitch.SwitchState<MenuState>(true);
            return Task.CompletedTask;
        }
    }
}
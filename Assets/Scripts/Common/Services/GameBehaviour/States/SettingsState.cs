using System.Threading.Tasks;
using Common.AssetLoaders;
using Common.AssetLoaders.Base;
using Common.Services.GameBehaviour.States.Base;
using Common.Services.SceneLoading.Constants;
using Settings.Startup;
using Settings.Widgets;
using UnityEngine;

namespace Common.Services.GameBehaviour.States
{
    public class SettingsState: BaseState
    {
        private readonly IAssetLoader<GameObject> _loader;
        private readonly SettingsWidgetFactory _settingsWidgetFactory;
        private SettingWidget _settingsWidget;
        
        public SettingsState(SettingsWidgetFactory settingsWidgetFactory,
            IStateSwitch stateSwitch) : base(stateSwitch)
        {
            _settingsWidgetFactory = settingsWidgetFactory;
            _loader = new AssetLoaderByKey<GameObject>(PrefabNames.SettingsPrefab);
        }

        public override async Task Start()
        {
            _settingsWidget = await _settingsWidgetFactory.Make(_loader);
        }

        public override Task Stop()
        {
            Object.Destroy(_settingsWidget.gameObject);
            _loader.Dispose();
            return Task.CompletedTask;
        }
        
        public override Task GoToMenu()
        {
            StateSwitch.SwitchState<MenuState>();
            return Task.CompletedTask;
        }

        public override Task GoToGameplay()
        {
            StateSwitch.SwitchState<GameplayState>();
            return Task.CompletedTask;
        }
    }
}
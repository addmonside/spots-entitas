using System.Threading.Tasks;

namespace Common.Services.GameBehaviour.States.Base
{
    public abstract class BaseState
    {
        protected readonly IStateSwitch StateSwitch;

        protected BaseState(IStateSwitch stateSwitch)
        {
            StateSwitch = stateSwitch;
        }

        public virtual Task Start()
        {
            return Task.CompletedTask;
        }

        public virtual Task Stop()
        {
            return Task.CompletedTask;
        }

        public virtual Task GoToMenu()
        {
            return Task.CompletedTask;
        }

        public virtual Task GoToGameplay()
        {
            return Task.CompletedTask;
        }

        public virtual Task GoToSettings()
        {
            return Task.CompletedTask;
        }
    }
}
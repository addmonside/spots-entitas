using System.Threading.Tasks;

namespace Common.Services.GameBehaviour.States.Base
{
    public interface IStateSwitch
    {
        Task SwitchState<T>(bool isSwitchStateThroughLoadingScene = false) where T : BaseState;
    }
}
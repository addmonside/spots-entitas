using System.Threading.Tasks;
using Common.AssetLoaders;

namespace Common.Services.GameBehaviour.States.Base
{
    public abstract class SceneState : BaseState
    {
        private readonly SceneLoader _loader;
        private bool _isSceneLoaded;
        protected bool IsUnloadScene;

        protected SceneState(string sceneName, IStateSwitch stateSwitch) : base(stateSwitch)
        {
            _loader = new SceneLoader(sceneName);
        }

        public override async Task Start()
        {
            if (_isSceneLoaded)
            {
                return;
            }
            
            await _loader.Load();
            _isSceneLoaded = true;
        }

        public override async Task Stop()
        {
            if (IsUnloadScene)
            {
                await _loader.Unload();
                _isSceneLoaded = false;
            }
        }
    }
}
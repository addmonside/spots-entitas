namespace Common.Services.GameBehaviour.States.Base
{
    public class EmptyState : BaseState
    {
        private static EmptyState _instance;
        public static EmptyState Instance => _instance ??= new EmptyState();
        private EmptyState() : base(null) {}
    }
}
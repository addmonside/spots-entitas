using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Common.AssetLoaders;
using Common.Services.GameBehaviour.States;
using Common.Services.GameBehaviour.States.Base;
using Common.Services.SceneLoading.Constants;
using Settings.Startup;

namespace Common.Services.GameBehaviour
{
    public class GameBehaviourService : IStateSwitch
    {
        private BaseState _prevState;
        private BaseState _currentState;
        private readonly Dictionary<BaseState, Action> _states;
        private readonly SceneLoader _loadingScene =  new(SceneNames.LoadingScene);

        public GameBehaviourService(SettingsWidgetFactory settingsWidgetFactory)
        {
            _states = new Dictionary<BaseState, Action>
            {
                {new StartupState(this), () => {} },
                {new MenuState(this), GoToMenu},
                { new GameplayState(this), GoToGameplay},
                {new SettingsState(settingsWidgetFactory, this), GoToSettings}
            };

            _currentState = GetStateByType(typeof(StartupState));
        }

        private BaseState GetStateByType(Type type)
        {
            return _states.Keys.FirstOrDefault(s => s.GetType() == type);
        }

        public void GoToMenu()
        {
            _currentState?.GoToMenu();
        }

        public void GoToGameplay()
        {
            _currentState?.GoToGameplay();
        }

        public void GoToSettings()
        {
            _currentState?.GoToSettings();
        }

        async Task IStateSwitch.SwitchState<T>(bool isSwitchStateThroughLoadingScene) 
        {
            async Task Switch()
            {
                _prevState = _currentState;
                var state = _states.Keys.ToList().FirstOrDefault(s => s is T) ?? EmptyState.Instance;
                await _currentState.Stop();
                await state.Start();
                _currentState = state;
            }
            
            if (isSwitchStateThroughLoadingScene)
            {
                await _loadingScene.Load();
                await Switch();
                await _loadingScene.Unload();
            } else {
                await Switch();
            }
        }

        public void GoToPrevState()
        {
            _states[_prevState].Invoke();
        }
    }
}
using System;
using Repository.Runtime;
using UnityEngine;

namespace Common.Services.Repository
{
    [Serializable]
    public class SettingsRepositoryService
    {
        private readonly IRepository _repository;

        public SettingsRepositoryService(IRepository repository)
        {
            _repository = repository;
        }
        
        public void Save<T>(T value) where T : class
        {
            var key = GetKey<T>();
            _repository.Save(key, value);
        }

        public void Load<T>(in T value) where T : class
        {
            var key = GetKey<T>();
            _repository.Load(key, value);
        }

        private static string GetKey<T>()
        {
            return $"{Application.productName}.{typeof(T)}";
        }
    }
}

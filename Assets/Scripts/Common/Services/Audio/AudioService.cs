using System;
using Asp.Runtime;
using Common.Services.Repository;
using Zenject;

namespace Common.Services.Audio
{
    [Serializable]
    public class AudioService : AudioPlayer, IInitializable, IDisposable
    {
        private readonly SettingsRepositoryService _settingsRepositoryService;

        public AudioService(SettingsRepositoryService settingsRepositoryService)
        {
            _settingsRepositoryService = settingsRepositoryService;
            ParametersChanged += SaveParameters;
        }

        private void SaveParameters()
        {
            _settingsRepositoryService.Save(this);
        }

        public void Initialize()
        {
            _settingsRepositoryService.Load(this);
        }

        public void Dispose()
        {
            ParametersChanged -= SaveParameters;
        }
    }
}
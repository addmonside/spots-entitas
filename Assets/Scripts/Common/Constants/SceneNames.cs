namespace Common.Services.SceneLoading.Constants
{
    public class SceneNames
    {
        public const string LoadingScene = "LoadingScene";
        public const string MenuScene = "MenuScene";
        public const string GameplayScene = "GameplayScene";
    }
}
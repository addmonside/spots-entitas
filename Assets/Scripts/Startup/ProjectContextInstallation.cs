using Asp.Runtime;
using Common.AssetLoaders.Base;
using Common.Services.Audio;
using Common.Services.GameBehaviour;
using Common.Services.Localization;
using Common.Services.Repository;
using Repository.Runtime.PlayerPrefs;
using Settings.Startup;
using Settings.Widgets;
using UnityEngine;
using Zenject;

namespace Startup
{
    public class ProjectContextInstallation : MonoInstaller
    {
        public override void InstallBindings()
        {
            BindMenuSettingsWidgetFactory();
            BindGameBehaviourService();
            BindSettingsRepositoryService();
            BindAudioService();
            BindLocaleIndexService();
        }

        private void BindMenuSettingsWidgetFactory()
        {
            Container
                .BindFactory<ILocaleIndexService, GameBehaviourService, IAudioParameters, IAssetLoader<GameObject>, SettingWidget, SettingsWidgetFactory>()
                .FromFactory<SettingsWidgetFactory>();
        }

        private void BindGameBehaviourService()
        {
            Container
                .Bind<GameBehaviourService>()
                .AsSingle()
                .NonLazy();
        }

        private void BindSettingsRepositoryService()
        {
            Container
                .Bind<SettingsRepositoryService>()
                .AsSingle()
                .WithArguments(new PlayerPrefsRepository())
                .Lazy();
        }

        private void BindAudioService()
        {
            Container
                .BindInterfacesTo<AudioService>()
                .AsSingle()
                .NonLazy();
        }

        private void BindLocaleIndexService()
        {
            Container
                .BindInterfacesTo<LocaleIndexService>()
                .AsSingle()
                .NonLazy();
        }
    }
}

using Common.Services.GameBehaviour;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Gameplay
{
    [RequireComponent(typeof(Button))]
    public class MenuButtonWidget : MonoBehaviour
    {
        private GameBehaviourService _gameBehaviourService;

        [Inject]
        private void Construct(GameBehaviourService gameBehaviourService)
        {
            _gameBehaviourService = gameBehaviourService;
        }
        
        private void Start()
        {
            var btn = GetComponent<Button>();
            btn.onClick.AddListener(GoToMenu);
        }

        private void GoToMenu()
        {
            _gameBehaviourService.GoToMenu();
        }
    }
}
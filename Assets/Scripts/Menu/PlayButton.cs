using Common.Services.GameBehaviour;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Menu
{
    [RequireComponent(typeof(Button))]
    public class PlayButton : MonoBehaviour
    {
        private GameBehaviourService _gameBehaviourService;

        [Inject]
        private void Construct(GameBehaviourService gameBehaviourService)
        {
            _gameBehaviourService = gameBehaviourService;
        }

        private void Start()
        {
            var btn = GetComponent<Button>();
            btn.onClick.AddListener(Play);
        }

        private void Play()
        {
            _gameBehaviourService.GoToGameplay();
        }
    }
}

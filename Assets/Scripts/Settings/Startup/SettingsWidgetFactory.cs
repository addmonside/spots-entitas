using System.Threading.Tasks;
using Asp.Runtime;
using Common.AssetLoaders.Base;
using Common.Services.GameBehaviour;
using Common.Services.Localization;
using Settings.Widgets;
using UnityEngine;
using Zenject;

namespace Settings.Startup
{
    public class SettingsWidgetFactory : PlaceholderFactory<ILocaleIndexService, GameBehaviourService, IAudioParameters, IAssetLoader<GameObject>, SettingWidget>
    {
        private readonly DiContainer _container;

        public SettingsWidgetFactory(DiContainer container)
        {
            _container = container;
        }
        
        public async Task<SettingWidget> Make(IAssetLoader<GameObject> loader)
        {
            var prefab = await loader.Load();
            var obj = _container.InstantiatePrefab(prefab);
            return obj.GetComponent<SettingWidget>();
            
        }
    }
}
using Common.Services.GameBehaviour;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Settings.Widgets
{
    public class SettingsButtonWidget : MonoBehaviour
    {
        [SerializeField] private Button _button;
        private GameBehaviourService _gameBehaviourService;

        [Inject]
        private void Construct(GameBehaviourService gameBehaviourService)
        {
            _gameBehaviourService = gameBehaviourService;
        }
        private void Start()
        {
            _button.onClick.AddListener(GoToSettings);
        }

        private void GoToSettings()
        {
            _gameBehaviourService.GoToSettings();
        }
    }
}
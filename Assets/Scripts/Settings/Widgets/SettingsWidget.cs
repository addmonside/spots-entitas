using Common.Services.GameBehaviour;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Settings.Widgets
{
    public class SettingWidget : MonoBehaviour
    {
        [SerializeField] private Button _closeButton;
        private GameBehaviourService _gameBehaviourService;

        [Inject]
        public void Construct(GameBehaviourService gameBehaviourService)
        {
            _gameBehaviourService = gameBehaviourService;
        }
        
        private void Start()
        {
            _closeButton.onClick.AddListener(CloseWidget);
        }

        private void CloseWidget()
        {
            _gameBehaviourService.GoToPrevState();
        }
    }
}
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Common.Services.Localization;
using TMPro;
using UnityEngine;
using UnityEngine.Localization.Settings;
using Zenject;

namespace Settings.Widgets
{
    [RequireComponent(typeof(TMP_Dropdown))]
    public class LanguageDropdownWidget : MonoBehaviour
    {
        private TMP_Dropdown _dropdown;
        private ILocaleIndexService _localeIndexService;

        [Inject]
        private void Construct(ILocaleIndexService  localeIndexService)
        {
            _localeIndexService = localeIndexService;
        }

        private IEnumerator Start()
        {
            yield return LocalizationSettings.InitializationOperation;
            
            var dropdown = GetComponent<TMP_Dropdown>();
            dropdown.options = GetAvailableLocales();
            dropdown.value = _localeIndexService.LocaleIndex;
            dropdown.onValueChanged.AddListener(LocaleSelected);
        }

        private static List<TMP_Dropdown.OptionData> GetAvailableLocales()
        {
            return LocalizationSettings.AvailableLocales.Locales
                .Select(locale => new TMP_Dropdown.OptionData(locale.name)).ToList();
        }

        private void LocaleSelected(int index)
        {
            _localeIndexService.LocaleIndex = index;
            LocalizationSettings.SelectedLocale = LocalizationSettings.AvailableLocales.Locales[index];
        }
    }
}
using Asp.Runtime;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

namespace Settings.Widgets
{
    public class AudioParametersWidget : MonoBehaviour
    {
        private IAudioParameters _audioParameters;
        [SerializeField] private Slider _musicSlider;
        [SerializeField] private Slider _soundSlider;

        [Inject]
        private void Construct(IAudioParameters audioParameters)
        {
            _audioParameters = audioParameters;
            Initialize();
        }

        private void Initialize()
        {
            _musicSlider.SetValueWithoutNotify(_audioParameters.MusicVolume);
            _musicSlider.onValueChanged.AddListener(ChangeMusicVolume);
            _soundSlider.SetValueWithoutNotify(_audioParameters.SfxVolume);
            _soundSlider.onValueChanged.AddListener(ChangeSoundVolume);
        }

        private void ChangeMusicVolume(float volume)
        {
            _audioParameters.MusicVolume = volume;
        }

        private void ChangeSoundVolume(float volume)
        {
            _audioParameters.SfxVolume = volume;
        }
    }
}